package com.goldenfrog.vyprvpn.vyprbdbtest.customview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.goldenfrog.vyprvpn.vyprbdbtest.R
import com.goldenfrog.vyprvpn.vyprbdbtest.models.Subscription
import kotlinx.android.synthetic.main.subscription_item_view.view.*


class SubscriptionItemView : ConstraintLayout {

    init {
        inflate(context, R.layout.subscription_item_view, this)
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    fun bindSubscriptionItem(item: Subscription) {
        original_title_value.text = item.title
        currency_value.text = item.currency
        price_value.text = item.price
        sku_value.text = item.sku
    }

    override fun setOnClickListener(l: OnClickListener?) {
        subscribe.setOnClickListener(l)
    }
}

package com.goldenfrog.vyprvpn.vyprbdbtest

import android.content.ActivityNotFoundException
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import com.android.billingclient.api.*
import com.goldenfrog.vyprvpn.vyprbdbtest.customview.SubscriptionItemView
import com.goldenfrog.vyprvpn.vyprbdbtest.models.Subscription
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(), PurchasesUpdatedListener {
    private val TAG = "MainActivity:BILLING"
    private var skuLoaded = false

    private val skuList = listOf(
        "test_monthly_sub",
        "test_annual_sub",
        "vyprvpn_premium_12_ttrial_vtrue_6999",
        "vyprvpn_premium_3_ttrial_vtrue_2499",
        "vyprvpn_premium_1_ttrial_vtrue_1199"
    )

    private val billingClient: BillingClient by lazy {
        BillingClient.newBuilder(this)
            .setListener(this)
            .enablePendingPurchases()
            .build()
    }
    private val coroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            Log.e(TAG, "coroutineExceptionHandler", throwable)
        }
    private val localCoroutineScope = CoroutineScope(Dispatchers.IO + coroutineExceptionHandler)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        bindListeners()
        connectToBillingClient()
    }

    private fun connectToBillingClient() {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    querySkuDetails()
                }
            }

            override fun onBillingServiceDisconnected() {
                logToUi("Billing service disconnected")
            }
        })
    }

    private fun querySkuDetails() {
        if (skuLoaded) return
        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)
        localCoroutineScope.launch {
            val result = billingClient.querySkuDetails(params.build())
            launch(Dispatchers.Main) {
                val skuDetailsList = result.skuDetailsList
                if (skuDetailsList.isNullOrEmpty()) {
                    generateTestSkus()
                    logToUi("It is mocked SKUs, real is empty")
                } else {
                    skuLoaded = true
                    bindSkuDetails(skuDetailsList.map { it.toSubscription() })
                }
            }
        }
    }

    private fun generateTestSkus() {
        bindSkuDetails(
            listOf(
                Subscription("Monthly", "UAH", "$11.99", "some_test_sku", Unit),
                Subscription("Annual", "UAH", "$99.99", "some_test_sku_annual", Unit)
            )
        )
    }

    private fun bindSkuDetails(skuDetailsList: List<Subscription>) {
        container.removeAllViews()
        skuDetailsList.forEach {
            val newItemView = SubscriptionItemView(this)
            newItemView.bindSubscriptionItem(it)
            newItemView.setOnClickListener { _ ->
                val skuDetails = it.skuDetails as? SkuDetails
                if (skuDetails != null) {
                    startBillingProcess(skuDetails)
                } else {
                    showSnackbar("skuDetails is null")
                }
                Log.d(TAG, "Subscribe to ${it.sku}")
            }
            container.addView(newItemView)
        }
    }

    private fun startBillingProcess(skuDetails: SkuDetails) {
        val flowParams = BillingFlowParams.newBuilder()
            .setSkuDetails(skuDetails)
            .build()
        val responseCode = billingClient.launchBillingFlow(this, flowParams).responseCode
    }

    private fun bindListeners() {
        policy_label.setOnClickListener { openWebPage("https://www.goldenfrog.com/privacy") }
        terms_label.setOnClickListener { openWebPage("https://www.goldenfrog.com/terms-of-service") }
    }

    private fun openWebPage(url: String?) {
        try {
            val webpage: Uri = Uri.parse(url)
            val myIntent = Intent(Intent.ACTION_VIEW, webpage)
            startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            showSnackbar("No application can handle this request. Please install a web browser or check your URL.")
            e.printStackTrace()
        }
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(scrollContainer, "$message\n", LENGTH_SHORT).show()
    }

    private fun SkuDetails.toSubscription(): Subscription {
        return Subscription(title, priceCurrencyCode, originalPrice, sku, this)
    }

    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: List<Purchase>?) {
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            for (purchase in purchases) {
                logToUi("New event, billingResult with responseCode BillingResponseCode.OK, \noriginalJson is ${purchase.originalJson}")
                handlePurchase(purchase)
            }
        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            logToUi("New event, billingResult with responseCode BillingResponseCode.USER_CANCELED")
        } else {
            logToUi("New event, with error in response code. Code is ${billingResult.responseCode}")
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged && check_acknowledge.isChecked) {
                val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                    .setPurchaseToken(purchase.purchaseToken)
                localCoroutineScope.launch {
                    billingClient.acknowledgePurchase(acknowledgePurchaseParams.build())
                    launch(Dispatchers.Main) {
                        logToUi("Purchase with orderId ${purchase.orderId} \n acknowledged successful")
                    }
                }
            }
        }
    }

    @UiThread
    private fun logToUi(message: String) {
        try {
            val newView = TextView(this)
            newView.text = message
            newView.setBackgroundResource(android.R.drawable.editbox_background)
            newView.setOnLongClickListener {
                showShareTextChooser(message)
                true
            }
            container.addView(newView)
        } catch (exception: Exception) {
            Log.e(TAG, "logToUi exception", exception)
        }
    }

    private fun showShareTextChooser(message: String) {
        try {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, message)
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        } catch (exception: Exception) {
            val clipboard: ClipboardManager? =
                getSystemService(CLIPBOARD_SERVICE) as? ClipboardManager
            val clip = ClipData.newPlainText("Debug message", message)
            clipboard?.setPrimaryClip(clip)
            showSnackbar("Copied to clipboard")
        }
    }
}
package com.goldenfrog.vyprvpn.vyprbdbtest.models

data class Subscription(
    val title: String,
    val currency: String,
    val price: String,
    val sku: String,
    val skuDetails: Any
)